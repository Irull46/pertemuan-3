import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import rn from '../assets/rn.png';

const Thumbnail = () => {
    return (
        <View style={setting.kontak2}>
            <Image source={rn} style={setting.banner} />
            <Text style={setting.text}>Judul Youtube</Text>
        </View>
    );
};

const setting = StyleSheet.create ({
    kontak2: {
        width: 288,
        backgroundColor: 'grey',
        padding: 10,
    },
    banner: {
        width: 268,
        height: 100,
        borderRadius: 5,
    },
    text: {
        color: '#fff',
        fontWeight: 'bold',
    },
});

export default Thumbnail;