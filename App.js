import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import logo from './assets/path4.png';
import Thumbnail from './components/Thumbnail';

const App = () => {
  return (
    <View style={setting.kotak}>
      <ScrollView>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Hello World</Text>
        <View style={setting.box} />
        <Image source={logo} style={setting.img} />
        <Thumbnail />
        <Thumbnail />
        <Thumbnail />
        <Thumbnail />
      </ScrollView>
    </View>
  );
};

const setting = StyleSheet.create({
  kotak: {
    padding: 10,
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#feca57',
    marginTop: 5,
  },
  img: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 5,
  },
});

export default App;
